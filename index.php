<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require (__DIR__ . '/vendor/autoload.php');

use App\Controllers\StudentController;
use App\Modals\Modal;

// Todo use mysql connection fromk Modal.php instead of this
$db_connection = mysqli_connect( Modal::DATABASE_HOST, Modal::DATABASE_USER_NAME, Modal::DATABASE_PASSWORD, Modal::DATABASE_NAME);

if (empty($_GET['student'])) {
    die('need ?student=$student_id inside request');
}

$modal = new Modal();
$student = new StudentController($_GET['student'],$db_connection);
$student->get_student_grades();
