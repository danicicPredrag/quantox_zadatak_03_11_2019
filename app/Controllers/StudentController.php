<?php

namespace App\Controllers;

use Exception;

use App\Modals\Student;
use App\Traits\student_result;

class StudentController //TODO add "extend class ClassName" For controllers that includes trowing exceptions and handling response
{
    use student_result;
    private $student_modal;
    private $mysql_connection;
    public function __construct($student_id, $mysql_connection)
    {
        // TODO make sure all modals are extending separate class for mysql connection
        $this->mysql_connection = $mysql_connection;
        $this->student_modal = new Student($student_id, $this->mysql_connection);
    }

    public function get_student_grades() {
        $student_grades = $this->student_modal->get_student_grades();
        if (empty($student_grades[0])) {
            //TODO add logger
            throw new Exception('THIS STUDENT HAVE NO GRADES');
        }

       $this->reformat_student_data_and_return_response($student_grades);
    }

}