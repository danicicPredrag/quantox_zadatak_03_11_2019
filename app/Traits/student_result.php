<?php

namespace App\Traits;

use App\Modals\Schools;
use SimpleXMLElement;

trait student_result
{

    public function get_average($student_grades)
    {
        $counter = 0;
        $total = 0;
        foreach ($student_grades as $key => $value) {
            $total = $total + $value['grade'];
            $counter++;
        }
        $average = $total / $counter;
        return $average;

    }

    public function calculate_result_CSM($average)
    {
        if ($average > 7) {
            return true;
        }
        return false;
    }

    public function return_response_JSON($student_data)
    {
        header("Content-Type: application/json");
        echo json_encode($student_data);
        die();
    }

    public function return_response_XML($student_data)
    {


        // this is not working IDK know why ... I never user Xml in my life and
        var_dump('xml will not work I never used it in my life and I already spent to much on it, I will fix it hopefully ');
        $student_data = json_decode(json_encode($student_data), true);
        $xml_string = '<?xml version="1.0" standalone="yes"?>';
        $xml_string .= '<student_data>';
        foreach ($student_data as $key => $value) {
            $xml_string .= '<' . $key . '>';
            //TODO male call with reference function here
            if (is_array($value)){
                foreach ($value as $key_2 => $value_2) {
                    $xml_string .= '<' . $key_2 . '>';
                    $xml_string .= $value_2;
                    $xml_string .= '</' . $key_2 . '>';
                }
            } else {
                $xml_string .= $value;
            }

            $xml_string .= '</' . $key . '>';
        }
        $xml_string .= '</student_data>';

        //        header('Content-Type:text/xml');
        $student_data_xml = new SimpleXMLElement($xml_string);
        print $student_data_xml->saveXML();
    }

    /**
     * This is not well defined in task ( why we discard lowest grade  here there is no point since for consider pass we are only looking for greater than 8)
     * - CSMB discards the lowest grade, if you have more than 2 grades, and considers pass if
     * his biggest grade is bigger than 8. Returns XML format
     */
    public function calculate_result_CSMB($student_grades)
    {
        foreach ($student_grades as $key => $val) {
            if ($val['grade'] > 8) {
                return true;
            }
        }
        return false;
    }

    // no point going back to controller since each board have exactly one type of response
    public function reformat_student_data_and_return_response($student_grades)
    {
        $average = $this->get_average($student_grades);
        $board_type = $student_grades[0]['school_board_type'];
        $grade_list = [];
        foreach ($student_grades as $key => $value) {
            $grade_list[] = $value['grade'];
        }
        $student_data = new \stdClass();
        $student_data->student_id = $student_grades[0]['student_id'];
        $student_data->name = $student_grades[0]['student_name'];
        $student_data->list_of_grades = $grade_list;
        $student_data->average_result = $this->get_average($student_grades);

        if ($board_type == Schools::SCHOOL_BOARD_TYPE_CSM) {
            $student_data->final_result = $this->calculate_result_CSM($average);
            $this->return_response_JSON($student_data);
        } else if ($board_type == Schools::SCHOOL_BOARD_TYPE_CSMB) {
            $student_data->final_result = $this->calculate_result_CSMB($student_grades);
            $this->return_response_XML($student_data);
        }

    }
}