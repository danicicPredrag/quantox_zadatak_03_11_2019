<?php

namespace App\Modals;

class Schools
{
    const TABLE_NAME = 'schools';
    const PRIMARY_KEY = 'school_id';

    const SCHOOL_BOARD_TYPE_CSM = 1;
    const SCHOOL_BOARD_TYPE_CSMB = 2;

}