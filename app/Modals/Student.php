<?php

namespace App\Modals;

class Student //TODO make sure you extend modal that have all connections to DB
{
    const TABLE_NAME = 'students';
    const PRIMARY_KEY = 'student_id';

    private $mysql_connection;
    private $student_id;
    public function __construct($student_id, $mysql_connection)
    {
        $this->student_id = $student_id;
        $this->mysql_connection = $mysql_connection;
    }

    public function get_student_grades() {
        $query = "sELECT `students`.`student_id`, `students`.`student_name`, `schools`.`school_board_type`, `student_classes_grades`.`grade` , `student_classes_grades`.`class_id` 
                    FROM `students`
                    JOIN `schools` ON `schools`.`school_id` =`students`.`school_id`
                    JOIN `student_classes_grades` ON `student_classes_grades`.`student_id` =`students`.`student_id`
                    WHERE  `students`.`student_id` = '$this->student_id' GROUP BY `student_classes_grades`.`class_id`;";
        $result = $this->mysql_connection->query($query);
        $student_grades = [];

        //TODO move this to same place where you keep mysql connection
        while ($row = $result->fetch_assoc()) {
            $student_grades[] = $row;
        }
        return $student_grades;

    }
}
