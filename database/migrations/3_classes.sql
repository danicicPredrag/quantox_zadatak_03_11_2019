CREATE TABLE `classes` (
  `class_id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `class_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  `updated_at` datetime NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE='InnoDB';