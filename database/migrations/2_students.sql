CREATE TABLE `students` (
  `student_id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `school_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  `updated_at` datetime NULL ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (`school_id`) REFERENCES `schools` (`school_id`) ON DELETE RESTRICT
) ENGINE='InnoDB';

ALTER TABLE `students`
ADD `student_name` varchar(255) NOT NULL AFTER `student_id`;