CREATE TABLE `schools` (
  `school_id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `school_name` varchar(255) COLLATE 'utf8_bin' NULL,
  `school_board_type` smallint(2) unsigned NOT NULL,
  `created_at` datetime NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT NOW()
);

ALTER TABLE `schools`
CHANGE `school_board_type` `school_board_type` smallint(2) unsigned NOT NULL COMMENT '\'CSM\' or \'CSMB\'' AFTER `school_name`;