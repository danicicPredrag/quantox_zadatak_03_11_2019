CREATE TABLE `student_classes_grades` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `grade` int NOT NULL,
  `created_at` datetime NOT NULL DEFAULT NOW(),
  `updated_at` datetime NULL ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (`student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE,
  FOREIGN KEY (`class_id`) REFERENCES `classes` (`class_id`) ON DELETE CASCADE
) ENGINE='InnoDB';