<?php
//TODO move database connection to one place in all project ( this is second place where you are placing i/t )
require (__DIR__ . '/../../vendor/autoload.php');

use App\Modals\Modal;

define("DATABASE_NAME", Modal::DATABASE_NAME);
define("DATABASE_HOST", Modal::DATABASE_HOST);
define("DATABASE_USER_NAME", Modal::DATABASE_USER_NAME);
define("DATABASE_PASSWORD", Modal::DATABASE_PASSWORD);

// THIS IS REALLY BASIC DATABASE SEEDER I HAD NO TIME TO MAKE THIS PROPERLY
$db_connection = mysqli_connect(DATABASE_HOST, DATABASE_USER_NAME, DATABASE_PASSWORD, DATABASE_NAME);

// Check connection
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}


// Create 10 classes
$class_suffix = 0;
for ($classes = 1; $classes <= 4; $classes++) {
// TODO MOVE name of tables so they are not hardcoded in queries ( as static constant in modals )
    $class_suffix++;
    $insert_classes_query = "INSERT INTO `classes` (`class_name`) VALUES ('class_name_" . $class_suffix . "')";
    $db_connection->query($insert_classes_query);
}

for ($school_ordinal = 1; $school_ordinal <= 10; $school_ordinal++) {

    $board_type = rand(1, 2);

    $insert_schools_query = "INSERT INTO `schools` (`school_name`,`school_board_type`) VALUES ('school_name_" . $school_ordinal . "', '" . $board_type . "')";
    $db_connection->query($insert_schools_query);


    for ($student_ordinal = 1; $student_ordinal <= 10; $student_ordinal++) {
        $student_name = 'student_name_' . $school_ordinal . "_" . $student_ordinal;
        // TODO make sure you are using propper school id instead of this "ordinal"
        $insert_student_query = "INSERT INTO `students` (`school_id`,`student_name`) VALUES ('" . $school_ordinal . "','" . $student_name . "')";
        $db_connection->query($insert_student_query);

        // I am using grades number as class_id bad practice but I wanted basic seeder for database .
        $grades_number = rand(1, 4);


        for ($grades_ordinal = 1; $grades_ordinal <= $grades_number; $grades_ordinal++) {
            $grade = rand(1, 10);
            $student_id = $school_ordinal - 1;
            $student_id = $student_id * 10;
            $student_id = $student_id + $student_ordinal;
            $insert_grade_query = "INSERT INTO `student_classes_grades` (
`student_id`,`class_id`,`grade`) VALUES ('" . $student_id . "','" . $grades_ordinal . "', '" . $grade . "')";
            $db_connection->query($insert_grade_query);
        }
    }
}